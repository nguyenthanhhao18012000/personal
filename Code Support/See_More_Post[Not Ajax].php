<?php

/**
 * URI: https://bitbucket.org/nguyenthanhhao18012000/personal/src/master/Code%20Support/See_More_Post[Not%20Ajax].php
 * Author: Nguyễn Thanh Hào
 * Author URI: https://www.facebook.com/Dev.Front/
 * Description: Code mẫu thiếu CSS 
 */


/**
 * Example: Ctrl + Click[Link]
 * https://drive.google.com/file/d/1hVFGvg_Sb2ckqPdyb41YXymgASAu7Hja/view?usp=sharing 
 */

$term = get_queried_object(); //Truy xuất đối tượng hiện đang được truy vấn.
$args_cat = array(
  'taxonomy' => $term->taxonomy,
  'parent'  => $term->term_id,
  'include_children' => true, // Lấy cate con True || False
  'order' => 'DESC',
);
$cats = get_categories($args_cat); // Get ALL name cate children
$limit = 3; // -1 để show all post không dùng xem thêm
global $wp_query;
global $post; ?>

<div>
  <h2><?php echo $term->name ?></h2>
  <div class="row">
    <?php
    if (empty($cats)) {
      echo '<h4>' . $term->name . '</h4>';
      $args = array(
        'post_type' => $post->post_type,
        'posts_per_page' => $limit,
        'post_status' => 'publish',
        'tax_query' => array(
          array(
            'taxonomy' => $term->taxonomy,
            'field' => 'id',
            'terms' => $term->term_id
          )
        )
      );
      $getposts = new WP_query($args);
      $wp_query->in_the_loop = true;
      while ($getposts->have_posts()) : $getposts->the_post();;
        $image = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
        <div>
          <a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>">
            <div><img src="<?php echo $image; ?>" alt="<?php echo get_the_title() ?>"></div>
            <h5><?php echo get_the_title() ?></h5>
          </a>
        </div>
      <?php endwhile;
      wp_reset_query();
    } else {
      foreach ($cats as $cat) : ?>
        <h4><?php echo $cat->name ?></h4>
        <?php
          $args = array(
            'post_type' => $post->post_type,
            'posts_per_page' => $limit,
            'post_status' => 'publish',
            'tax_query' => array(
              array(
                'taxonomy' => $term->taxonomy,
                'field' => 'id',
                'terms' => $cat->term_id
              )
            )
          );
        $getposts = new WP_query($args);
        $wp_query->in_the_loop = true;
        while ($getposts->have_posts()) : $getposts->the_post();
          $image = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
          <div>
            <a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>">
              <div><img src="<?php echo $image; ?>" alt="<?php echo get_the_title() ?>"></div>
              <h5><?php echo get_the_title() ?></h5>
            </a>
          </div>
        <?php endwhile;
        if ($getposts->found_posts / $limit >= 1) {
        ?>
          <div>
            <a href="<?php echo get_category_link($cat->term_id) ?>">
              <h5>Xem thêm</h5>
            </a>
          </div>
        <?php
        }
        wp_reset_query();
        ?>
    <?php endforeach;
    }
    if ($term->description) {
      echo '<div><p>' . $term->description . '</p></div>';
    }
    ?>
  </div>
</div>