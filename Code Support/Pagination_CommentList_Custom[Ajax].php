<?php

/**
 * URI: https://bitbucket.org/nguyenthanhhao18012000/personal/src/master/Code%20Support/Pagination.php
 * Author: Nguyễn Thanh Hào
 * Author URI: https://www.facebook.com/Dev.Front/
 * Description: Code mẫu thiếu CSS 
 */


/**
 * Example: Ctrl + Click[Link]
 * https://drive.google.com/file/d/1zPwFrHicVdVPHWpZtBv6SDL_sMCUamZO/view?usp=sharing
 * Description: Click vào Value Pagination sẽ tiến hành xóa bỏ listcomment - Phân trang hiện tại và cho show listcomment - Phân trang mới
 */


$id = get_the_ID();
$paginates =  c_parent_comment_counter($id);
$per_page = 10;
$page = 1;
?>
<ol class="comment-list" data-total="<?php echo $paginates ?>" data-id="<?php echo $id ?>" data-perpage="<?php echo $per_page ?>">
  <div class="before-comment-list-custom"></div>
  <div class="comment-list-custom" data-paged="1">
    <?php
    $comments = get_comments(array(
      'post_id' => $id,
      'status' => 'approve'
    ));
    wp_list_comments(
      array(
        'style'      => 'ol',
        'short_ping' => true,
        'callback'      => "list_comment_admission",
        'per_page' => $per_page,
        'page' => $page,
      ),
      $comments
    );
    echo '<div class="commentPagination">';
    echo paginate_links(array(
      'base' => add_query_arg('paged', '%#%'),
      'format' => '',
      'prev_text' =>  __('&laquo;'),
      'next_text' => __('&raquo;'),
      'total' => ceil($paginates / $per_page),
      'current' => $page
    ));
    echo '</div>';
    ?>
  </div>
</ol><!-- .comment-list -->

<?php

// init link admin ajax

add_action('wp_head', '_fn_init_ajax');
function _fn_init_ajax()
{
?>
  <script>
    var BASE_AJAX_URL = "<?php echo admin_url('admin-ajax.php'); ?>";
  </script>
<?php
}


// Get id user comment by Display Name
function get_user_id_by_display_name($display_name)
{
  global $wpdb;
  if (!$user = $wpdb->get_row($wpdb->prepare(
    "SELECT `ID` FROM $wpdb->users WHERE `display_name` = %s",
    $display_name
  )))
    return false;

  return $user->ID;
}


// Custom list comment
function list_comment_admission($comment, $args, $depth)
{
  $GLOBALS['comment'] = $comment; ?>
  <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
    <div id="comment-<?php comment_ID(); ?>" class="comment-wrap">
      <div class="comment-head comment-author vcard">
        <div class="comment-content comment-text">
          <?php if ($comment->comment_approved == '0') { ?>
            <em><?php _e('Bình luận đang chờ kiểm duyệt.'); ?></em><br />
          <?php }
          comment_text(); ?>
          <i class="fas fa-caret-down accordion"></i>
        </div>
        <div class="info-user">
          <?php echo get_avatar($comment, 32);
          $user = get_userdata(get_user_id_by_display_name(get_comment_author())); // Get the user object.
          ?>
          <div class="title-question_details-comments">
            <?php
            if (!empty($user)) {
              $user_roles = $user->roles;
              if (in_array('subscriber', $user_roles, true)) {
                echo '<span class="name_author_comment">' . get_comment_author() . '</span>';
              }
              if (in_array('administrator', $user_roles, true)) {
                echo '<span class="name_author_comment">Ban tuyển sinh</span>';
              }
            } else {
              echo '<span class="name_author_comment">' . get_comment_author() . '</span>';
            }
            ?>
            <span class="comment-date">
              <?php printf(__('%1$s - %2$s'), 'đã gửi lúc ' . get_comment_time('h:i'), get_comment_date('d\/m\/Y')); ?>
            </span>
          </div>
        </div>
      </div>
    <?php // kết thúc không cần dùng thẻ </div></i>
  } ?>

    <script>
      jQuery(document).ready(function($) {
        commentPagination(); // active
        removeHref(); // active
      });

      // Loại bỏ Link Pagination Default
      function removeHref() {
        jQuery('.commentPagination a').removeAttr("href");
      }

      function commentPagination() {
        jQuery('body').on('click', '.page-numbers', function() {
          var e = jQuery(this).text();
          var id = jQuery('.comment-list').data('id');
          var total = jQuery('.comment-list').data('total');
          var perpage = jQuery('.comment-list').data('perpage');
          //xử lý click symbol » next page comment
          if (e == '»')
            e = parseInt(jQuery('.page-numbers.current').text()) + 1;
          //xử lý click symbol « prev page comment
          if (e == '«')
            e = parseInt(jQuery('.page-numbers.current').text()) - 1;

          var data = {
            'action': 'pagination_comment',
            'e': e,
            'id': id,
            'total': total,
            'perpage': perpage,
          };
          jQuery.ajax({
            url: BASE_AJAX_URL,
            type: 'POST',
            data: data,
          }).done(function(data) {
            jQuery('.comment-list-custom').remove();
            jQuery('.commentPagination').remove();
            jQuery('.before-comment-list-custom').after(data);
            removeHref();
            jQuery("body,html").animate({
              scrollTop: jQuery('.before-comment-list-custom').offset().top - 100
            })
          }).fail(function() {
            alert('Error')
          })

        });
      }
    </script>