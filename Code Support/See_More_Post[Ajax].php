<?php

/**
 * URI: https://bitbucket.org/nguyenthanhhao18012000/personal/src/master/Code%20Support/See_More_Post[Ajax].php
 * Author: Nguyễn Thanh Hào
 * Author URI: https://www.facebook.com/Dev.Front/
 * Description: Code mẫu thiếu CSS 
 */


/**
 * Example: Ctrl + Click[Link]
 * https://drive.google.com/file/d/1SC-zocOUEC0pn6ZD_w6lR9PPM6NlF-VM/view?usp=sharing
 * Description: Mỗi lần load post tăng thêm theo biến $limit
 */

global $post;
$term = get_queried_object();
$limit = 12;
$args = array(
  'post_status' => 'publish',
  'taxonomy' => $term->taxonomy,
  'post_type' => $post->post_type,
  'posts_per_page' => $limit,
  'paged'  => 1,
  'post__not_in' => get_option('sticky_posts'), // Lấy post sticky hoặc không(xóa dòng này)
);
$getposts = new WP_query($args);
global $wp_query;
$wp_query->in_the_loop = true;
if (have_posts()) : ?>
  <div class="post-list row" data-limit="<?php echo $limit ?>" data-total="<?php echo ceil($getposts->found_posts / $limit); ?>" data-tax="<?php echo $term->taxonomy ?>" data-type="<?php echo $post->post_type ?>">
    <?php /* Start the Loop */ ?>
    <?php while ($getposts->have_posts()) : $getposts->the_post();
      $image = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
      <div id="post-<?php the_ID(); ?>">
        <a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>">
          <div><img src="<?php echo $image; ?>" alt="<?php echo get_the_title() ?>"></div>
          <h5><?php echo get_the_title() ?></h5>
        </a>
      </div>
    <?php endwhile;
    wp_reset_query(); ?>
  </div>
  <?php if (($getposts->found_posts) > $limit) : ?>
    <div class="work__button-container">
      <button id="js-work__btn-view-more" class="font_link-more">Xem thêm</button>
    </div>
  <?php endif;
endif;

// Init link admin ajax
add_action('wp_head', '_fn_init_ajax');
function _fn_init_ajax()
{ ?>
  <script>
    var BASE_AJAX_URL = "<?php echo admin_url('admin-ajax.php'); ?>";
  </script>
  <?php
}


//  Ajax load more post
add_action('wp_ajax_my_action', 'my_action');
add_action('wp_ajax_nopriv_my_action', 'my_action');
function my_action()
{
  $args = array(
    'post_status' => 'publish',
    'post_type' => $_POST['type'],
    'posts_per_page' => $_POST['limit'],
    'paged' => $_POST['page'],
    'taxonomy' => $_POST['tax'],
    'post__not_in' => get_option('sticky_posts'),  // Lấy post sticky hoặc không(xóa dòng này)
  );
  $getposts = new WP_query($args);
  while ($getposts->have_posts()) : $getposts->the_post();
    $image = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
    <div id="post-<?php the_ID(); ?>">
      <a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>">
        <div><img src="<?php echo $image; ?>" alt="<?php echo get_the_title() ?>"></div>
        <h5><?php echo get_the_title() ?></h5>
      </a>
    </div>
<?php endwhile;
  wp_reset_query();
  wp_die();
}
?>

<script>
  // Func Load More Post Single
  function loadPost() {
    var page = 2;
    var total = jQuery('.post-list').data('total');
    var tax = jQuery('.post-list').data('tax');
    var type = jQuery('.post-list').data('type');
    var limit = jQuery('.post-list').data('limit');
    jQuery('#js-work__btn-view-more').click(function() {
      var data = {
        'action': 'my_action',
        'page': page,
        'tax': tax,
        'type': type,
        'limit': limit,
      };
      jQuery.ajax({
        url: BASE_AJAX_URL,
        type: 'POST',
        data: data,
      }).done(function(data) {
        if (total == page) {
          jQuery('.work__button-container').hide();
        }
        jQuery('.post-list').append(data);
        page++;
      }).fail(function() {
        alert("Error")
      })
    });
  }
</script>