<?php

/**
 * URI: https://bitbucket.org/nguyenthanhhao18012000/personal/src/master/Code%20Support/Pagination.php
 * Author: Nguyễn Thanh Hào
 * Author URI: https://www.facebook.com/Dev.Front/
 * Description: Code mẫu thiếu CSS 
 */


/**
 * Example: Ctrl + Click[Link]
 * https://drive.google.com/file/d/1QuzQDk3EN5dDqlaC52C6xL-3J8nr0jsu/view?usp=sharing
 * Description: Click vào Value Pagination sẽ tiến hành xóa bỏ Bài đăng và Phân trang hiện tại và cho show Bài đăng và Phân trang mới
 */


$term = get_queried_object(); // Truy xuất đối tượng hiện tại
global $post;
$limit = 9;
$args = array(
  'post_type' => $post->post_type,
  'posts_per_page' => $limit,
  'paged'    => 1,
  'tax_query' => array(
    array(
      'taxonomy' => $term->taxonomy,
      'field'    => 'id',
      'terms'    => $term->term_id,
    ),
  ),
);

$getposts = new WP_query($args);
global $wp_query;
$wp_query->in_the_loop = true;
?>
<div class="before-post" data-type="<?php echo $post->post_type ?>" data-total="<?php echo $getposts->found_posts ?>" data-term="<?php echo $term->term_id ?>" data-perpage="<?php echo $limit ?>" data-tax="<?php echo $term->taxonomy ?>"></div>
<?php
echo '<div class="_paginate" data-paged="1">';
while ($getposts->have_posts()) : $getposts->the_post();
  $image = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
  <div id="post-<?php the_ID(); ?>">
    <a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>">
      <div><img src="<?php echo $image; ?>" alt="<?php echo get_the_title() ?>"></div>
      <h5><?php echo get_the_title() ?></h5>
    </a>
  </div>
<?php endwhile;
wp_reset_query();


echo '<div class="Pagination">';
echo '<a class="prev-custom page-numbers">&laquo;</a>';
echo paginate_links(array(
  'base' => add_query_arg('paged', '%#%'),
  'format' => '',
  'prev_text' =>  false,
  'next_text' => false,
  'total' => ceil($getposts->found_posts / $limit),
  'current' => 1
));
echo '<a class="next-custom page-numbers">&raquo;</a>';
echo '</div>';
echo '</div>';



// init link admin ajax

  add_action('wp_head', '_fn_init_ajax');
  function _fn_init_ajax()
  {
    ?>
      <script>
        var BASE_AJAX_URL = "<?php echo admin_url('admin-ajax.php'); ?>";
      </script>
    <?php
  }



// ajax Pagination list
add_action('wp_ajax_pagination_post', 'pagination_post');
add_action('wp_ajax_nopriv_pagination_post', 'pagination_post');
function pagination_post()
{
?>
  <?php
  global $post;
  $args = array(
    'post_type' => $_POST['type'],
    'posts_per_page' => $_POST['perpage'],
    'paged'  => $_POST['e'],
    'tax_query' => array(
      array(
        'taxonomy' => $_POST['tax'],
        'field'    => 'id',
        'terms'    => $_POST['term'],
      ),
    ),
  );
  ?>
  <?php $getposts = new WP_query($args);
  global $wp_query;
  $wp_query->in_the_loop = true;
  echo '<div class="_paginate" data-paged="' . $_POST['e'] . '">';
  while ($getposts->have_posts()) : $getposts->the_post();
    $image = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
    <div id="post-<?php the_ID(); ?>">
      <a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>">
        <div><img src="<?php echo $image; ?>" alt="<?php echo get_the_title() ?>"></div>
        <h5><?php echo get_the_title() ?></h5>
      </a>
    </div>
<?php endwhile;
  wp_reset_query();
  echo '<div class="Pagination">';
  echo '<a class="prev-custom page-numbers">&laquo;</a>';
  echo paginate_links(array(
    'base' => add_query_arg('paged', '%#%'),
    'format' => '',
    'prev_text' =>  false,
    'next_text' => false,
    'total' => ceil($_POST['total']  / $_POST['perpage']),
    'current' => $_POST['e'],
  ));
  echo '<a class="next-custom page-numbers">&raquo;</a>';
  echo '</div>';
  echo '</div>';

  wp_die();
}
?>

<script>
  jQuery(document).ready(function($) {

    removeHref();
    if (jQuery('.before-post').data('type') == 'post') {
      Pagination('pagination_post');
    }
    if (jQuery('.before-post').data('type') == 'faqs') {
      Pagination('pagination_faqs');
    }
    disable_Prev_Next();
    checkTotalPage();
    turnOff_breadcrumb();

  });



  // Cả 3 func removeHref,disable_Prev_Next,checkTotalPage đều có thể dùng chung nên cho truyền biến nếu ở Posttype nào thì dùng func.php của PostType đó


  // Loại bỏ Link Pagination Default
  function removeHref() {
    jQuery('.Pagination a').removeAttr("href");
  }

  function Pagination(ajax) { 
    jQuery('body').on('click', '.page-numbers', function() {
      var e = jQuery(this).text();
      var term = jQuery('.before-post').data('term');
      var total = jQuery('.before-post').data('total');
      var perpage = jQuery('.before-post').data('perpage');
      var tax = jQuery('.before-post').data('tax');
      var type = jQuery('.before-post').data('type');
      //xử lý click symbol » next page comment
      if (e == '»')
        e = parseInt(jQuery('.page-numbers.current').text()) + 1;
      //xử lý click symbol « prev page comment
      if (e == '«')
        e = parseInt(jQuery('.page-numbers.current').text()) - 1;
      var data = {
        'action': ajax,
        'e': e,
        'term': term,
        'total': total,
        'perpage': perpage,
        'tax': tax,
        'type': type,
      };
      jQuery.ajax({
        url: BASE_AJAX_URL,
        type: 'POST',
        data: data,
      }).done(function(data) {
        jQuery('._paginate').remove();
        jQuery('.before-post').after(data);
        disable_Prev_Next();
        removeHref();
        jQuery("body,html").animate({
          scrollTop: jQuery('.before-post').offset().top - 300
        }); // Tự scroll lên đầu element -300(để phục vụ cho web có MenuTop động)
      }).fail(function() {
        alert('Error')
      })

    });
  }

  function disable_Prev_Next() {
    var paged = jQuery('._paginate').data('paged');
    var maxPaged = Math.ceil(jQuery('.before-post').data('total') / jQuery('.before-post').data('perpage'));
    if (paged == 1) {
      jQuery('.Pagination .prev-custom').addClass('disable'); // CSS     pointer-events: none; cho đối tượng nằm trong điều kiện dc thêm Class=[disable]
    }
    if (paged == maxPaged) {
      jQuery('.Pagination .next-custom').addClass('disable');
    }
  }

  function checkTotalPage() {
    var total = jQuery('.before-post').data('total');
    var perpage = jQuery('.before-post').data('perpage');
    if (total <= perpage) {
      jQuery('.Pagination .prev-custom').hide();
      jQuery('.Pagination .next-custom').hide();
    }
  }
</script>